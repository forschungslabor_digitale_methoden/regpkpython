# -*- coding: utf-8 -*-
import scrapy
import datetime, locale
from scrapy.utils.markup import remove_tags

class ArchivBundesregierungSpider(scrapy.Spider):
    # name of scraper
    name = 'archiv_bundesregierung'
    # area of scraping
    allowed_domains = ['archiv.bundesregierung.de']
    # prefix for transcriptions links
    main_url = "https://archiv.bundesregierung.de"
    # start at specific page
    start_url = "https://archiv.bundesregierung.de/archiv-de/dokumente/69986!search?page=720"
    # string must be contained to collect transcription
    links_regpk_selector = "regierungspressekonferenz"

    def start_requests(self):
        """
        Initially make request to first archive page and process then in parse_search_items
        :return: scrapy.request[]
        """
        yield scrapy.Request(self.start_url, callback=self.parse_search_items)

    def parse_search_items(self, response):
        """
        Create request for parsed search items and retrieve further search items
        :param response: scrapy.response
        :return: scrapy.request[]
        """
        # search next page button
        next_page_container = response.xpath('//li[@class="forward"]/a/@href')

        if next_page_container:
            # if next page button present then create request for next page with retrieved url
            url_postfix = next_page_container.extract_first()
            yield scrapy.Request("{}{}".format(self.main_url, url_postfix), callback=self.parse_search_items)

        # create requests for all search items linked to the regpk_selector
        search_items_container = response.xpath('//li/h3/a/@href').extract()
        for link in search_items_container:
            if self.links_regpk_selector in link:
                yield scrapy.Request("{}{}".format(self.main_url, link))

    def parse(self, response):
        """
        Scrape transcriptions page and get the wanted properties
        :param response:
        :return: dict (href:string, shorttext:string, fulltext: string[], date: datetime)
        """

        # xpath selector for shorttext
        regpk_shorttext = response.xpath('//div[@class="abstract"]').extract_first()
        regpk_shorttext = remove_tags(regpk_shorttext) # have to use remove_tags since /text() has a bug in this case -> will be reported to parsel
        regpk_shorttext = regpk_shorttext.replace(u'\xa0', ' ').strip() # cleaning

        # xpath selector for transcription chunks
        regpk_fulltext = response.xpath('//div[@class="basepage_pages"]/*[not(self::div)]//text()').extract()
        # using list comprehension to clean text from artifacts and empty entries
        regpk_fulltext = [item.replace(u'\xa0', ' ') for item in regpk_fulltext if not "" == item.strip()]

        # xpath selector for date
        regpk_time_extracted = response.xpath('//p[@class="date"]/text()').extract_first()
        # parse date with regular expression and german locale FIXME: do only once?
        locale.setlocale(locale.LC_ALL, "german")
        regpk_time = datetime.datetime.strptime(regpk_time_extracted.split(",")[1].strip(), '%d. %B %Y')

        # return scraped fields
        yield {"href": response.url, "shorttext": regpk_shorttext, "fulltext": regpk_fulltext, "date": regpk_time.strftime("%Y-%m-%d")}